﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VideoController : MonoBehaviour
{
    public UMP.UniversalMediaPlayer mUmp;
    public GameObject mSkyFlyLogo;
    public static VideoController instance;
    // Start is called before the first frame update
    void Awake()
    {
        instance = this;
    }

    private void Start() {
        //mUmp.GetComponent<UMP.UniversalMediaPlayer>();
        
        mUmp = GameObject.FindObjectOfType<UMP.UniversalMediaPlayer>();
        if (mUmp != null ) {
            mUmp.Path = Application.streamingAssetsPath + "/hap.mov";
        mUmp.Prepare();
        }
    }
    public void PlayVideo() {
        mUmp.Play();
    }

    public void StopVideo() {
        mUmp.Pause();
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.K)&& Input.GetKeyDown(KeyCode.F)) {
            ToggleLogo();
        }
    }

    void ToggleLogo() {
        print(mSkyFlyLogo.activeSelf);
        mSkyFlyLogo.SetActive(!mSkyFlyLogo.activeSelf);
    }
}
