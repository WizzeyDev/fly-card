﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadingUmp : MonoBehaviour {

    public UMP.UniversalMediaPlayer ump;

    public void switchPlayPause()
    {
        if (ump.IsPlaying)
            ump.Pause();
        else ump.Play();
    }
	// Update is called once per frame
}
